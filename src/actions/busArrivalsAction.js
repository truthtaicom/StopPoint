import { checkStatus, parseJSON, headers } 	from '../utils';
import { API } 	                            from '../utils/constants';

export const GET_BUS_ARRIVALS_REQUEST = 'GET_BUS_ARRIVALS_REQUEST';
export const GET_BUS_ARRIVALS_SUCCESS = 'GET_BUS_ARRIVALS_SUCCESS';
export const GET_BUS_ARRIVALS_FAILURE = 'GET_BUS_ARRIVALS_FAILURE';

export function getBusArrivalsAction(id) {

    const URL = `${API.BASE}/StopPoint/${id}/arrivals`;

	return (dispatch) => {
        dispatch({ type: GET_BUS_ARRIVALS_REQUEST });

        return fetch(URL, { method: 'GET', headers })
            .then(checkStatus)
            .then(parseJSON)
            .then(data 			=> dispatch({ type: GET_BUS_ARRIVALS_SUCCESS, data }))
            .catch(error 		=> dispatch({ type: GET_BUS_ARRIVALS_FAILURE, error }));
	}
}