import { checkStatus, parseJSON, headers } 	from '../utils';
import { API } 	                            from '../utils/constants';

export const GET_STOP_POINT_REQUEST = 'GET_STOP_POINT_REQUEST';
export const GET_STOP_POINT_SUCCESS = 'GET_STOP_POINT_SUCCESS';
export const GET_STOP_POINT_FAILURE = 'GET_STOP_POINT_FAILURE';

export function getStopPointAction(id) {

    const URL = `${API.BASE}/line/${id}/route/sequence/outbound`;

	return (dispatch) => {
		dispatch({ type: GET_STOP_POINT_REQUEST });

        return fetch(URL, { method: 'GET', headers })
            .then(checkStatus)
            .then(parseJSON)
            .then(data 			=> dispatch({ type: GET_STOP_POINT_SUCCESS, data }))
            .catch(error 		=> dispatch({ type: GET_STOP_POINT_FAILURE, error }));
        }
}