import { Router }                                 from 'react-native-router-flux';
import { createStore, applyMiddleware, compose }  from 'redux';
import { connect }                                from 'react-redux';
import thunkMiddleware                            from "redux-thunk";
import createLogger                               from "redux-logger";
import reducers                                   from './reducers';

const RouterWithRedux = connect()(Router);

const loggerMiddleware = createLogger({ predicate: () => '__DEV__' });

const middleware = [thunkMiddleware, loggerMiddleware];

const store = compose(
  applyMiddleware(...middleware)
)(createStore)(reducers);

export default store;