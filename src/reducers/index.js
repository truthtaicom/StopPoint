import { combineReducers } from 'redux';
import busArrivalsReducer from './busArrivalsReducer';
import stopPointReducer from './stopPointReducer';
import routesReducer from './routesReducer';

const rootReducer = combineReducers({ routesReducer, busArrivalsReducer, stopPointReducer });

export default rootReducer;