import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ListView,
  Platform,
  RefreshControl,
  Dimensions,
} from 'react-native';
import React, { Component, PropTypes }  from 'react';
import { bindActionCreators }           from 'redux';
import { connect }                      from 'react-redux';
import { Actions }                      from "react-native-router-flux";
import Icon                             from 'react-native-vector-icons/FontAwesome';
import {
  getBusArrivalsAction,
  busArrivalsAction
}                                       from '../actions';
import Spinner                          from 'react-native-loading-spinner-overlay';
import { STYLE }                        from '../utils/variables';

const {width, height} = Dimensions.get('window');

class BusArrivals extends Component {

    constructor(props) {
      super(props);
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
        refreshing: false,
        dataSource: ds.cloneWithRows(this.props.busArrivalsReducer.busArrivals)
      };
    }

    _onRefresh() {
      this.props.getBusArrivalsAction(this.props.busArrivalsReducer.busArrivals[0].naptanId);
    }
  
    _renderRow(data) {        
      return (
        <TouchableOpacity>
          <View style={styles.row}>
            <View style={styles.description}>
                <View style={styles.vehicle}>
                    <Icon name='bus' color='#666' size={15} />
                    <Text style={[styles.textBold, styles.textLine]}> { data.lineId.toUpperCase() }</Text>
                    <Text style={[styles.textBold, styles.textStation]}> { data.destinationName }</Text>
                </View>
                <View style={styles.arrival}>
                    <Icon name='clock-o' color='#666' size={15} />
                    <Text style={styles.textSub}>{ parseInt(data.timeToStation / 60) } m</Text>
                </View>
            </View>
          </View>
        </TouchableOpacity>
      )
    }

    render() {
        return (
            <View style={styles.container}>
                <ListView
                  dataSource={this.state.dataSource}
                  renderRow={(rowData) => this._renderRow(rowData) }
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={() => this._onRefresh()}
                    />
                  }
                />
            </View>
    )
  }
}

BusArrivals.propTypes = {
  getBusArrivalsAction: PropTypes.func
}

const stateToProps = (state) => {
  return {
    busArrivalsReducer: state.busArrivalsReducer
  }
}

const dispatchToProps = (dispatch) => {
  return bindActionCreators({getBusArrivalsAction}, dispatch)
}

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    flex: 1,
    backgroundColor: '#F5FCFF',
    flexDirection:'column',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderColor: '#ddd',
    borderBottomWidth: 1,
    marginVertical: 3,
    backgroundColor: '#fff'
  },
  description: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    flex: 0.7,
  },
  textBold: {
    fontWeight: 'bold',
  },
  textSub: {
    flex: 0.3,
    fontSize: 13,
    textAlign: 'right'
  },
  textLine: {
    width: 50
  },
  textStation: {
    fontWeight: '400',
    color: STYLE.grey
  },
  vehicle: {
    flex: 0.6,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  arrival: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  iconReadMore: {
    width: 30,
    height: 30
  }
})

export default connect(stateToProps, dispatchToProps)(BusArrivals)