import {
    GET_BUS_ARRIVALS_REQUEST,
    GET_BUS_ARRIVALS_SUCCESS,
    GET_BUS_ARRIVALS_FAILURE,
} from '../actions';

const initialState = {
    busArrivals: null,
    busArrivalsGetting: false,
    busArrivalsGettingError: false
};

function initializeState() {
    return Object.assign({}, initialState);
}

export default function busArrivalsReducer(state = initializeState(), action = {}) {
    switch (action.type) {

        case GET_BUS_ARRIVALS_REQUEST:
            return {
                ...state,
                busArrivals: null,
                busArrivalsGetting: true
            };

        case GET_BUS_ARRIVALS_SUCCESS:
            return {
                ...state,
                busArrivals: action.data,
                busArrivalsGetting: false
            };

        case GET_BUS_ARRIVALS_FAILURE:
            return {
                ...state,
                busArrivalsGettingError: action.error,
                busArrivalsGetting: false
            };

        default:
            return state;
    }
}